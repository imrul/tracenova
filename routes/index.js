var express = require('express');
var reader  = require('../models/reader');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/read', function (req, res, next) {

    var result = reader.readXL('G:/NodeJs/Tracenova/tracenova/public/storage/scb-mar16.xlsx');

    res.send(result);
});

module.exports = router;
