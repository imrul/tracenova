var xlsx = require('node-xlsx');
var fs   = require('fs');

module.exports.readXL = function (xlfile, callback) {

    try{
        const workSheetsFromBuffer = xlsx.parse(fs.readFileSync(xlfile));
        var workSheetJson = JSON.stringify(workSheetsFromBuffer);
            //console.log('In reader: '+workSheetJson);
        return workSheetJson;

    }catch (e){
        console.log('Error in reading XL file--> '+e.toString());
        return callback;
    }
}

